import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.fftpack import fft
from util import *

#Print CWD
import os
print(os.getcwd())

BASE_PATH = "./../Data/"
sampling_frequency = 6.25e6  # Example: 6.25 MHz, replace with your actual sampling frequency

L19x4 = computeSignalParameters(BASE_PATH+'19PSC4PRD1.csv', sampling_frequency, title='19PSC4PRD1', xlims=(0, 3.6125))
L19x9 = computeSignalParameters(BASE_PATH+'19PSC9PRD1.csv', sampling_frequency, title='19PSC9PRD1', xlims=(0, 6.125))
L19x19 = computeSignalParameters(BASE_PATH+'19PSC19PRD1.csv', sampling_frequency, title='19PSC19PRD1', xlims=(0, 12.5))

L38x4 = computeSignalParameters(BASE_PATH+'38PSC4PRD1.csv', sampling_frequency, title='38PSC4PRD1', xlims=(0, 6.125))
L38x9 = computeSignalParameters(BASE_PATH+'38PSC9PRD1.csv', sampling_frequency, title='38PSC9PRD1', xlims=(0, 12.5))
L38x19 = computeSignalParameters(BASE_PATH+'38PSC19PRD1.csv', sampling_frequency, title='38PSC19PRD1', xlims=(0, 25))

L152x4 = computeSignalParameters(BASE_PATH+'152PSC4PRD1.csv', sampling_frequency, title='152PSC4PRD1', xlims=(0, 25))
L152x9 = computeSignalParameters(BASE_PATH+'152PSC9PRD1.csv', sampling_frequency, title='152PSC9PRD1', xlims=(0, 50))
L152x19 = computeSignalParameters(BASE_PATH+'152PSC19PRD1.csv', sampling_frequency, title='152PSC19PRD1', xlims=(0, 100))

print(L19x4)
print(L19x9)
print(L19x19)
print(" ")

print(L38x4)
print(L38x9)
print(L38x19)
print(" ")

print(L152x4)
print(L152x9)
print(L152x19)

multiPlot(BASE_PATH+'19PSC4PRD1.csv', BASE_PATH+'19PSC9PRD1.csv', BASE_PATH+'19PSC19PRD1.csv', sampling_frequency, title='Multi19', xlims=(0, 12.5))
multiPlot(BASE_PATH+'38PSC4PRD1.csv', BASE_PATH+'38PSC9PRD1.csv', BASE_PATH+'38PSC19PRD1.csv', sampling_frequency, title='Multi38', xlims=(0, 25))
multiPlot(BASE_PATH+'152PSC4PRD1.csv', BASE_PATH+'152PSC9PRD1.csv', BASE_PATH+'152PSC19PRD1.csv', sampling_frequency, title='Multi152', xlims=(0, 100))


