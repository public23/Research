import pandas as pd
from scipy.fftpack import fft
from scipy.signal import find_peaks
from scipy.fft import fft
import numpy as np
import matplotlib.pyplot as plt


def compute_snr(signal, sampling_frequency):
    # Compute FFT of the signal
    N = len(signal)
    yf = fft(signal)
    xf = np.fft.fftfreq(N, 1 / sampling_frequency)

    # Plot the FFT of the signal (magnitude) REAL PART in DB
    """
    plt.figure()
    plt.plot(xf[:N // 2], 20 * np.log10(np.abs(yf[:N // 2]) / N))
    plt.grid()
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Magnitude')
    plt.title('FFT of the signal')
    plt.show()
    """

    # Find the fundamental frequency
    fundamental_freq = xf[np.argmax(np.abs(yf))]

    # Find the noise floor
    noise_floor = np.mean(20 * np.log10(np.mean(np.abs(yf[1:]) / N)))

    # Find the peak of the fundamental frequency
    fundamental_power = 20 * np.log10(np.abs(yf[np.argmax(np.abs(yf))] / N))

    # Calculate SNR
    snr = fundamental_power - noise_floor

    return snr


def compute_thd(signal, sampling_rate):
    # Perform FFT
    N = len(signal)
    yf = fft(signal)
    xf = np.fft.fftfreq(N, 1 / sampling_rate)

    # Find the fundamental frequency
    fundamental_freq = xf[np.argmax(np.abs(yf))]

    # Compute power of the fundamental frequency
    fundamental_power = np.abs(yf[np.argmax(np.abs(yf))]) ** 2 / N

    # Compute power of harmonics
    harmonics_power = 0
    for i in range(2, 6):  # Considering up to the 5th harmonic
        harmonic_freq = i * fundamental_freq
        harmonic_index = np.argmin(np.abs(xf - harmonic_freq))
        harmonics_power += np.abs(yf[harmonic_index]) ** 2 / N

    # Calculate THD
    thd = np.sqrt(harmonics_power / fundamental_power)

    return thd


def computeSignalParameters(file, sampling_frequency, title, xlims):
    data1 = pd.read_csv(file, skiprows=10)
    data1['Channel 1 (V)'] = pd.to_numeric(data1['Channel 1 (V)'], errors='coerce').fillna(0)
    data1_array = data1['Channel 1 (V)'].to_numpy()

    # Offset removal
    data1_array = data1_array - np.mean(data1_array)

    # FFT of the signal
    yf = fft(data1_array)
    N = len(data1_array)

    # Frequency axis for FFT
    xf = np.fft.fftfreq(N, 1 / sampling_frequency)

    # Calculate power spectrum (magnitude squared of FFT)
    power_spectrum = np.abs(yf) ** 2

    # Find the fundamental frequency peak and harmonic peaks
    peaks, _ = find_peaks(power_spectrum[:N // 2], height=np.max(power_spectrum[:N // 2]) * 0.01)

    if len(peaks) == 0:
        return {"Error": "No significant peaks found in the spectrum"}

    # Fundamental frequency is the first peak
    fundamental_idx = peaks[0]
    fundamental_power = power_spectrum[fundamental_idx]

    # Harmonic power: sum of all harmonic components (we assume next peaks are harmonics)
    harmonic_power = np.sum(power_spectrum[peaks[1:]])

    # Vrms calculation
    Vrms = np.sqrt(np.mean(data1_array ** 2))

    # Crest Factor
    CrestFactor = np.max(np.abs(data1_array)) / Vrms

    SNR = compute_snr(data1_array, sampling_frequency)

    # THD (Total Harmonic Distortion) - based on harmonics
    THD = 100*compute_thd(data1_array, sampling_frequency)

    # Generate time X array for plotting
    time = 1000000*np.linspace(0, len(data1_array) / sampling_frequency, len(data1_array))

    # Plot the signal
    plt.figure()
    plt.plot(time, data1_array, '-o', markersize=5, markerfacecolor='r', markeredgecolor='r')
    plt.grid()
    plt.xlabel(r'$time (\mu S)$')
    # Limit x axis
    plt.xlim(xlims[0], xlims[1])
    plt.ylabel('Amplitude (V)')
    plt.title('Signal')
    plt.savefig(title+".png")

    # Compute FFT of the signal
    N = len(data1_array)
    yf = fft(data1_array)
    xf = np.fft.fftfreq(N, 1 / sampling_frequency)

    # Plot the FFT of the signal (magnitude) REAL PART in DB
    plt.figure()
    plt.plot(xf[1:N // 2], 20 * np.log10(np.abs(yf[1:N // 2]) / N))
    plt.grid()
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Magnitude')
    plt.title('FFT')
    plt.savefig(title+"_FFT.png")

    # Compute fundamental frequency
    fundamental_freq = xf[np.argmax(np.abs(yf))]

    # Return all computed values
    return {
        "Crest Factor": CrestFactor,
        "SNR": SNR,
        "THD": THD,
        "F" : fundamental_freq,
    }


def computeSignalParameters2(data1_array, sampling_frequency, title, xlims):
    # FFT of the signal
    yf = fft(data1_array)
    N = len(data1_array)

    # Frequency axis for FFT
    xf = np.fft.fftfreq(N, 1 / sampling_frequency)

    # Calculate power spectrum (magnitude squared of FFT)
    power_spectrum = np.abs(yf) ** 2

    # Find the fundamental frequency peak and harmonic peaks
    peaks, _ = find_peaks(power_spectrum[:N // 2], height=np.max(power_spectrum[:N // 2]) * 0.01)

    if len(peaks) == 0:
        return {"Error": "No significant peaks found in the spectrum"}

    # Fundamental frequency is the first peak
    fundamental_idx = peaks[0]
    fundamental_power = power_spectrum[fundamental_idx]

    # Harmonic power: sum of all harmonic components (we assume next peaks are harmonics)
    harmonic_power = np.sum(power_spectrum[peaks[1:]])

    # Vrms calculation
    Vrms = np.sqrt(np.mean(data1_array ** 2))

    # Crest Factor
    CrestFactor = np.max(np.abs(data1_array)) / Vrms

    SNR = compute_snr(data1_array, sampling_frequency)

    # THD (Total Harmonic Distortion) - based on harmonics
    THD = 100*compute_thd(data1_array, sampling_frequency)


    # Generate time X array for plotting
    time = 1000000*np.linspace(0, len(data1_array) / sampling_frequency, len(data1_array))

    # Plot the signal
    plt.figure()
    plt.plot(time, data1_array, '-o', markersize=2, markerfacecolor='r', markeredgecolor='r')
    plt.grid()
    plt.xlabel(r'$time (\mu S)$')
    # Limit x axis
    plt.xlim(xlims[0], xlims[1])
    plt.ylabel('Amplitude (V)')
    plt.savefig(title+".png")

    # Compute FFT of the signal
    N = len(data1_array)
    yf = fft(data1_array)
    xf = np.fft.fftfreq(N, 1 / sampling_frequency)

    # Plot the FFT of the signal (magnitude) REAL PART in DB
    plt.figure()
    plt.plot(xf[1:N // 2], 20 * np.log10(np.abs(yf[1:N // 2]) / N))
    plt.grid()
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Magnitude')
    plt.title('FFT')
    plt.savefig(title+"_FFT.png")

    # Compute fundamental frequency
    fundamental_freq = xf[np.argmax(np.abs(yf))]

    # Compute Amplitude of fundamental frequency
    fundamental_amplitude = 2*np.sqrt(2)*np.abs(yf[np.argmax(np.abs(yf))]) / N

    # Return all computed values
    return {
        "Crest Factor": CrestFactor,
        "SNR": SNR,
        "THD": THD,
        "F" : fundamental_freq,
        "A" : fundamental_amplitude
    }


def multiPlot(file1, file2, file3, sampling_frequency, title, xlims):
    data1 = pd.read_csv(file1, skiprows=10)
    data2 = pd.read_csv(file2, skiprows=10)
    data3 = pd.read_csv(file3, skiprows=10)

    data1['Channel 1 (V)'] = pd.to_numeric(data1['Channel 1 (V)'], errors='coerce').fillna(0)
    data2['Channel 1 (V)'] = pd.to_numeric(data2['Channel 1 (V)'], errors='coerce').fillna(0)
    data3['Channel 1 (V)'] = pd.to_numeric(data3['Channel 1 (V)'], errors='coerce').fillna(0)

    data1_array = data1['Channel 1 (V)'].to_numpy()
    data2_array = data2['Channel 1 (V)'].to_numpy()
    data3_array = data3['Channel 1 (V)'].to_numpy()

    # Offset removal
    data1_array = data1_array - np.mean(data1_array)
    data2_array = data2_array - np.mean(data2_array)
    data3_array = data3_array - np.mean(data1_array)

    # Generate time X array for plotting
    time = 1000000*np.linspace(0, len(data1_array) / sampling_frequency, len(data1_array))

    # Compute FFT of the signal
    N1 = len(data1_array)
    yf1 = fft(data1_array)
    xf1 = np.fft.fftfreq(N1, 1 / sampling_frequency)

    N2 = len(data2_array)
    yf2 = fft(data2_array)
    xf2 = np.fft.fftfreq(N2, 1 / sampling_frequency)

    N3 = len(data3_array)
    yf3 = fft(data3_array)
    xf3 = np.fft.fftfreq(N3, 1 / sampling_frequency)

    # Create a figure 2x3
    fig, axs = plt.subplots(3, 2)
    plt.tight_layout()

    # Select the first subplot
    plt.sca(axs[0, 0])
    plt.plot(time, data1_array, '-o', markersize=1, markerfacecolor='r', markeredgecolor='r')
    plt.grid()
    plt.xlabel(r'time $(\mu S)$')
    plt.xlim(xlims[0]/4, xlims[1]/4)

    plt.title('Time Domain')
    plt.ylabel('Amplitude (V)')

    plt.sca(axs[0, 1])
    plt.plot(xf1[1:N1 // 2], 20 * np.log10(np.abs(yf1[1:N1 // 2]) / N1))
    plt.grid()
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Magnitude')
    plt.title('Frequency Domain')
    plt.savefig(title+"_FFT.png")


    # Select the second subplot
    plt.sca(axs[1, 0])
    plt.plot(time, data2_array, '-o', markersize=1, markerfacecolor='r', markeredgecolor='r')
    plt.grid()
    plt.xlabel(r'time $(\mu S)$')
    plt.xlim(xlims[0]/2, xlims[1]/2)
    plt.ylabel('Amplitude (V)')

    plt.sca(axs[1, 1])
    plt.plot(xf2[1:N2 // 2], 20 * np.log10(np.abs(yf2[1:N2 // 2]) / N2))
    plt.grid()
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Magnitude')

    # Select the third subplot
    plt.sca(axs[2, 0])
    plt.plot(time, data2_array, '-o', markersize=1, markerfacecolor='r', markeredgecolor='r')
    plt.grid()
    plt.xlabel(r'time $(\mu S)$')
    plt.xlim(xlims[0], xlims[1])
    plt.ylabel('Amplitude (V)')

    plt.sca(axs[2, 1])
    plt.plot(xf3[1:N3 // 2], 20 * np.log10(np.abs(yf3[1:N3 // 2]) / N3))
    plt.grid()
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Magnitude')
    plt.tight_layout()
    plt.savefig(title+"_FFT.png")



def multiPlot2Signals(data1_array, data2_array, sampling_frequency, title, xlims):
    # Create a figure 2x3
    fig, axs = plt.subplots(2, 2)

    # Generate time X array for plotting
    time = 1000000*np.linspace(0, len(data1_array) / sampling_frequency, len(data1_array))

    # Plot the signal
    plt.figure()
    plt.tight_layout()
    plt.sca(axs[0, 0])
    plt.plot(time, data1_array, '-o', markersize=2, markerfacecolor='r', markeredgecolor='r')
    plt.grid()
    plt.xlabel(r'$time (\mu S)$')
    # Put y ticks every 0.5 V
    plt.yticks(np.arange(-2, 2, 0.5))
    # Limit x axis
    plt.xlim(xlims[0], xlims[1])
    plt.ylabel('Amplitude (V)')
    plt.title('Time Domain')

    plt.sca(axs[1, 0])
    plt.plot(time, data2_array, '-o', markersize=2, markerfacecolor='r', markeredgecolor='r')
    plt.grid()
    plt.xlabel(r'$time (\mu S)$')
    # Put y ticks every 0.05 V
    plt.yticks(np.arange(-0.20, 0.20, 0.05))
    # Limit x axis
    plt.xlim(xlims[0], xlims[1])
    plt.ylabel('Amplitude (V)')

    # Compute FFT of the signal
    yf1 = fft(data1_array)
    N1 = len(data1_array)
    xf1 = np.fft.fftfreq(N1, 1 / sampling_frequency)

    yf2 = fft(data2_array)
    N2 = len(data2_array)
    xf2 = np.fft.fftfreq(N2, 1 / sampling_frequency)


    plt.sca(axs[0, 1])
    plt.plot(xf1[1:N1 // 2], 20 * np.log10(np.abs(yf1[1:N1 // 2]) / N1))
    plt.grid()
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Magnitude')
    plt.title('Frequency Domain')

    plt.sca(axs[1, 1])
    plt.plot(xf2[1:N2 // 2], 20 * np.log10(np.abs(yf2[1:N2 // 2]) / N2))
    plt.grid()
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Magnitude')
    plt.tight_layout()

    plt.savefig(title+".png")