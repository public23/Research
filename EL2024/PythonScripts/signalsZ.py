import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.fftpack import fft
from util import *

BASE_PATH = "./../Data/"
sampling_frequency = 1e6  # Example: 6.25 MHz, replace with your actual sampling frequency



# Read BASE_PATH+'Test100K.csv' and plot the signals
data1 = pd.read_csv(BASE_PATH+'Test100K.csv', skiprows=10)
data1['Channel 1 (V)'] = pd.to_numeric(data1['Channel 1 (V)'], errors='coerce').fillna(0)
data1['Channel 2 (V)'] = pd.to_numeric(data1['Channel 2 (V)'], errors='coerce').fillna(0)

data1_array = data1['Channel 1 (V)'].to_numpy()
data2_array = data1['Channel 2 (V)'].to_numpy()

# Offset removal
data1_array = data1_array - np.mean(data1_array)
data2_array = data2_array - np.mean(data2_array)

T1001 = computeSignalParameters2(data1_array, sampling_frequency, title='Test100K1', xlims=(0, 300))
T1002 = computeSignalParameters2(data2_array, sampling_frequency, title='Test100K2', xlims=(0, 300))

print(T1001)
print(T1002)

multiPlot2Signals(data1_array, data2_array, sampling_frequency, title='Multi100K', xlims=(0, 300))
