/*
 * currentGenerator.h
 *
 *  Created on: Oct 13, 2024
 *      Author: pablo
 */

#ifndef INC_CURRENTGENERATOR_H_
#define INC_CURRENTGENERATOR_H_

void initializeSystem(void);
void createLUT(uint32_t * buffer, float amplitude, uint16_t size);
void setFrequency(float frequency);

#endif /* INC_CURRENTGENERATOR_H_ */
