/*
 * currentGenerator.c
 *
 *  Created on: Oct 13, 2024
 *      Author: pablo
 */
#include "main.h"
#include <math.h>
#include "currentGenerator.h"

extern DAC_HandleTypeDef hdac3;

extern OPAMP_HandleTypeDef hopamp3;
extern OPAMP_HandleTypeDef hopamp4;
extern OPAMP_HandleTypeDef hopamp5;
extern OPAMP_HandleTypeDef hopamp6;

extern TIM_HandleTypeDef htim6;

void initializeSystem(void){
	//Timers
	HAL_TIM_Base_Init(&htim6);
	HAL_TIM_Base_Start(&htim6);

	//OPAMS
	HAL_OPAMP_Start(&hopamp6);
	HAL_OPAMP_Start(&hopamp4);
	HAL_OPAMP_Start(&hopamp3);
	HAL_OPAMP_Start(&hopamp5);
}

void createLUT(uint32_t * buffer, float amplitude, uint16_t size){

	uint16_t amp = (uint16_t) (amplitude*2048);
	float step = 2*M_PI/size;
	uint32_t i = 0;
	while(i < size){
		buffer[i] = (uint16_t) ((sin(i * step)*amp/sin(2*M_PI/4))+amp);
		i++;
	}
}

void setFrequency(float frequency){

}

void generateSignal(uint32_t *lut, uint16_t lut_size){
	//DAC
	HAL_DACEx_DualSetValue(&hdac3, DAC_ALIGN_12B_R, 0, 0);
	HAL_DACEx_DualStart_DMA(&hdac3, DAC_CHANNEL_1, (uint32_t*)lut,lut_size,DAC_ALIGN_12B_R);

	HAL_Delay(10);		// Wait 10ms so that the signal is stable.

}
